artifactory_url="http://localhost:8081/artifactory"
echo $artifactory_url
repo="libs-snapshot-local"
echo $repo
artifacts="com/agiletestingalliance/cpdofwebappsep/CPDOFWebAppSep19"
echo $artifacts
name=$artifacts
echo $name
url=$artifactory_url/$repo/$artifacts
echo "url" $url
file=`curl -s -u admin:password $url/maven-metadata.xml`
echo $file
version=`curl -s -u admin:password $url/maven-metadata.xml | grep latest | sed "s/.*<latest>\([^<]*\)<\/latest>.*/\1/"`
echo "version" $version
build=`curl -s -u admin:password $url/$version/maven-metadata.xml | grep '<value>' |head -1 | sed "s/.*<value>\([^<]*\)<\/value>.*/\1/"`
BUILD_LATEST="$url/$version/CPDOFWebAppSep19-$build.war"
echo "File Name  = " $BUILD_LATEST
echo $BUILD_LATEST > filename.txt
